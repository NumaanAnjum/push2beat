<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Api extends Controller
{

	public function index()
	{
	
		$name = Request::get('name');
		echo $name;
		echo '<br>';
		echo "This is index function.we need to print variables ....";


	}





	public function SignUp()
	{	
		    $validation = Validator::make(Request::all(),[ 
	        'firstname' => 'required',
	        'lastname'  => 'required',
	        'email'     => 'required',
	    	'lattitude' => 'required',
	    	'longitude' => 'required',
	        'password'  => 'required',
	    ]);

	    
	    if($validation->fails())
	    	{

	    		$finalResult = array('code' => 100,
					'msg' => 'Field Not Found',
					'data' => array()
					);

	    	}
	     else
	     	{

	     		$user = User::where('email', '=', Input::get('email'))->first();
				
				if (is_null($user))
				
				  {
					$user = new User();
		     	    $user->first_name  = Request::get('firstname');
		     	    $user->last_name   = Request::get('lastname');
		     	    $user->email       = Request::get('email');
		     	    $user->lattitude   = Request::get('lattitude');
		     	    $user->longitude   = Request::get('longitude');
		     	    $user->password    = password(Request::get('password')) ;
		     	    $user->save();

		     		$finalResult = array('code' => 200,
						'msg' => 'New User Created',
						'data' => array()
				    		);
	   			  }
		     	 else
		     	 {

		     		$finalResult = array('code' => 100,
					'msg' => 'email is already taken.',
					'data' => array()
					);


		     	 }
	     		
	     		  
	            	        	
	    	}

	    	echo json_encode($finalResult);

		}




		public function Social_SignUp()
	{	
		    $validation   = Validator::make(Request::all(),[ 
	        'firstname'   => 'required',
	        'lastname'    => 'required',
	        'email'       => 'required',
	    	'lattitude'   => 'required',
	    	'longitude'   => 'required',
	        'social_token'=> 'required',
	    ]);

	    
	    if($validation->fails())
	    	{

	    		$finalResult = array('code' => 100,
					'msg' => 'Failure',
					'data' => array()
					);

	    	}
	     else
	     	{

	     	$user = User::where('email', '=', Input::get('email'))->first();
				
				if (is_null($user))
				
				  {
					$user = new User();
		     	    $user->first_name  = Request::get('firstname');
		     	    $user->last_name   = Request::get('lastname');
		     	    $user->email       = Request::get('email');
		     	    $user->lattitude   = Request::get('lattitude');
		     	    $user->longitude   = Request::get('longitude');
		     	    $user->social_token= Request::get('social_token');
		     	    $user->save();

		     		$finalResult = array('code' => 200,
						'msg' => 'New User Created',
						'data' => array()
				    		);
	   			  }
		     	 else
		     	 {

		     		$finalResult = array('code' => 100,
					'msg' => 'email is already taken.',
					'data' => array()
					);


		     	 }
	     		
	     		  
	            	        	
	    	}

	    	echo json_encode($finalResult);

		}


	public function Signin()
	{
		 $validation = Validator::make(Request::all(),[ 
	        'email'        => 'required',
	        'password'     => 'required',
	        'device_type'  => 'required',
	        'device_token' => 'required',
	        
	    ]);

	    
	    if($validation->fails())
	     {

	    		$finalResult = array('code' => 100,
					'msg' => 'Data Entered Not Correct.',
					'data' => array()
					);

	     }

	   else
	     {
	     	   $login = User::where(
		     	   	[
					    ['email', '=', Input::get('email')],
					    ['password', '=', md5(Input::get('password'))],
					])->first();


	
			   if (is_null($login))
			{

				$finalResult = array('code' => 100,
					'msg' => 'Your Account Does not exist.',
					'data' => array()
					);
			
			}

			else
			{


				$user= User::where('email', '=', Input::get('email'))->first();
					$user->device_token = Input::get('device_token');
					$user->device_type = Input::get('device_type');

					$user->save();


				$data = User::where(
					 [ 'email'    =>Input::get('email')],
					 [ 'password' =>md5(Input::get('password'))]
					 )->get();


				$finalResult = array('code' => 200,
					'msg' => 'Success',
					'data' => $data
					);

			}

		}	

	       	echo json_encode($finalResult);

	}


	public function Social_Signin()
	{
		 $validation = Validator::make(Request::all(),[ 
	        'social_token'     => 'required',
	        
	    ]);

	    
	    if($validation->fails())
	     {

	    		$finalResult = array('code' => 100,
					'msg' => 'Data Entered Not Correct.',
					'data' => array()
					);

	     }

	   else
	     {
	     	   $login = User::where(
		     	   	[
					    ['social_token', '=', Input::get('social_token')],
					])->first();


	
			   if (is_null($login))
			{

				$finalResult = array('code' => 100,
					'msg' => 'Failure',
					'data' => array()
					);
			
			}

			else
			{

				$data = User::where(
					 [ 'social_token'=>Input::get('social_token')]
					 )->get();

				$finalResult = array('code' => 100,
					'msg' => 'Success',
					'data' => $data
					);

			}

		}	

	       	echo json_encode($finalResult);

	}



	public function update_location()
	{

		$validation = Validator::make(Request::all(),[ 
	        'id'         => 'required',	
	        'lattitude'  => 'required',
	        'longitude'  => 'required',
	        
	    ]);

	    
	    if($validation->fails())
	     {

	    		$finalResult = array('code' => 100,
					'msg' => 'Data Entered Not Correct.',
					'data' => array()
					);

	     }

	   else
	     {

	     	$user = User::where(
		     	   	[
					    ['id', '=', Input::get('id')],
					])->first();


	
			if ( is_null($user) )
			 {
 				 	$finalResult = array('code' => 100,
					'msg' => 'Your Account Does not exist.',
					'data' => array()
					);
			
			}

			else
			{

				$user = User::find(Input::get('id'));
					$user->lattitude = Input::get('lattitude');
					$user->longitude = Input::get('longitude');

					$user->save();


            		            		
            $finalResult = array('code' => 200,
						'msg' => 'Location Updated',
						'data' => array()
				    		);			

			}


		}


       	echo json_encode($finalResult);

		
	}



	public function get_profile()
	{
		 $validation = Validator::make(Request::all(),[ 
	        'id'     => 'required',
	       
	        
	    ]);

	    
	    if($validation->fails())
	     {

	    		$finalResult = array('code' => 100,
					'msg' => 'Data Entered Not Correct.',
					'data' => array()
					);

	     }

	   else
	     {
	     	   $login = User::where(
		     	   	[
					    ['id', '=', Input::get('id')],
					    
					])->first();


	
			   if (is_null($login))
			{

				$finalResult = array('code' => 100,
					'msg' => 'Your Account Does not exist.',
					'data' => array()
					);
			
			}

			else
			{

				$data = User::where(
					 [ 'id'    =>Input::get('id')]
					  )->get();


				$finalResult = array('code' => 200,
					'msg' => 'Success',
					'data' => $data
					);

			}

		}	

	       	echo json_encode($finalResult);

	}

	





}
