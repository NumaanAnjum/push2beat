<?php

namespace App\Http\Controllers;

use Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class Track extends Controller
{
    


	public function addtrack()
	{	
		    $validation = Validator::make(Request::all(),[ 
	        'total_time'      => 'required',
	        'genrated_by'     => 'required',
	        'distance'        => 'required',
	    	'start_lattitude' => 'required',
	    	'start_longitude' => 'required',
	        'end_lattitude'   => 'required',
	        'end_longitude'   => 'required',
	        	        
	    ]);

	    
	    if($validation->fails())
	    	{

	    		$finalResult = array('code' => 100,
					'msg' => 'Invalid Data Entered ',
					'data' => array()
					);

	    	}
	     else
	     	{
	     		$track = new \App\Track();
		     	    $track->total_time       = Request::get('total_time');
		     	    $track->genrated_by      = Request::get('genrated_by');
		     	    $track->distance         = Request::get('distance');
		     	    $track->start_longitude  = Request::get('start_longitude');
		     	    $track->start_lattitude  = Request::get('start_lattitude');
		     	    $track->end_lattitude    = Request::get('end_lattitude');
		     	    $track->end_longitude    = Request::get('end_lattitude');

		     	    $track->save();

		     		$finalResult = array('code' => 200,
						'msg' => 'New Track Created',
						'data' => array()
				    		);
	   			  }

	       	echo json_encode($finalResult);

	     	}




	public function gettracks()
	     	{
	     		
	     	 $validation = Validator::make(Request::all(),[ 
	         'user_id'     => 'required',
   	         'track_id'     => 'required',

	       
	        
	    ]);

	    
	    if($validation->fails())
	     {

	    		$finalResult = array('code' => 100,
					'msg' => 'Data Recieved Not Correct.',
					'data' => array()
					);

	     }

	   else
	     {
	     	$data = \App\Track::where(
		     	   	[
					    ['genrated_by', '=', Input::get('user_id')],
					    ['id', '=',Input::get('track_id') ],
					])->get();


    	     	$finalResult = array('code' => 100,
					'msg' => 'Your Account is found.',
					'data' => $data
					);

	     }

	       	echo json_encode($finalResult);


	}













}
