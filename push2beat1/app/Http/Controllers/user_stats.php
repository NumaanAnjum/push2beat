<?php

namespace App\Http\Controllers;
use Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class user_stats extends Controller

{
    

	public function add_stats()
	{	
		    $validation = Validator::make(Request::all(),[ 
	        'user_id'            => 'required',
	        'caleries_goal'      => 'required',
	        'caleries_current'   => 'required',
	        'distance_goal'      => 'required',
	    	'distance_current'   => 'required',
	    		        	        
	    ]);

	    
	    if($validation->fails())
	    	{

	    		$finalResult = array('code' => 100,
					'msg' => 'Invalid Data Entered ',
					'data' => array()
					);

	    	}
	     else
	     	{
	     		$stat = new \App\stat();
		     	    $stat->user_id          = Request::get('user_id');
		     	    $stat->caleries_current = Request::get('caleries_current');
		     	    $stat->caleries_goal    = Request::get('caleries_goal');
		     	    $stat->distance_current  = Request::get('distance_current');
		     	    $stat->distance_goal  = Request::get('distance_goal');
		     	  
		     	    $stat->save();

		     		$finalResult = array('code' => 200,
						'msg' => 'User Stats stored',
						'data' => array()
				    		);
	   			  }

	       	echo json_encode($finalResult);

	     	}




        public function update_stats()
	    
	     {	
		    $validation = Validator::make(Request::all(),[ 
	        'user_id'          => 'required',
	        'caleries_burned'  => 'required',
	    	'distance_ran'     => 'required',
	    		        	        
	    ]);

	    
	    if($validation->fails())
	    	{

	    		$finalResult = array('code' => 100,
					'msg' => 'Invalid Data Entered ',
					'data' => array()
					);

	    	}
	     else
	     	{

	     	$user = \App\stat::where(
		     	   	[
					    ['user_id', '=', Input::get('user_id')],
					])->first();


	
			if ( is_null($user) )
			 {
 				 	$finalResult = array('code' => 100,
					'msg' => 'Your Account Does not exist.',
					'data' => array()
					);
			
			}

			else
			{

	     
	     		$finalResult = array('code' => 100,
					'msg' => 'Record Updated.',
					'data' => array()
					);

	   	    }

	   	}


   		       	echo json_encode($finalResult);




   	}


}
