<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');




// User Profile Routes 

Route::post('/signup', 'Api@signup');
Route::post('/socialsignup', 'Api@Social_SignUp');
Route::post('/login', 'Api@Signin');
Route::post('/sociallogin', 'Api@Social_Signin');
Route::post('/updatelocation', 'Api@update_location');
Route::post('/getprofile', 'Api@get_profile');



//Tracks Related Routes 
Route::post('/addtrack', 'Track@addtrack');
Route::post('/gettracks', 'Track@gettracks');



// Routes For the User Stats 
Route::post('/addstats', 'user_stats@add_stats');
Route::post('/updatestats', 'user_stats@update_stats');


